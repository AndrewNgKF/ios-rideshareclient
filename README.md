### WIP ###

This is a project that connects drivers and passengers who wish to save money on transportation costs. 

Upon starting up the app, the passenger will be able to see all nearby drivers, and put up a ride request. 

Upon doing so, all nearby drivers with pickup mode enabled will be able to receive the ride request and may choose to accept.

When the driver is connected to the passenger, the driver will receive directions to the passenger's current location.

Firebase manages all accounts, and the location update feature is managed by its real time database. 


## Upcoming ##

Verification

Trip start and end distance and time

Figure out an algorithm that calculates suggested cost contribution for the fare

in-app messaging betweeen driver and passenger upon accepting ride

more advanced driver and passenger profile

keep track of trips and distance travelled for points

rating system for driver and passenger










