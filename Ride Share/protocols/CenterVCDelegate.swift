//
//  File.swift
//  Ride Share
//
//  Created by Drewton on 22/1/18.
//  Copyright © 2018 Andrew Ng. All rights reserved.
//

import UIKit

protocol CenterVCDelegate {
    func toggleLeftPanel()
    func addLeftPanelViewController()
    func animateLeftPanel(shouldExpand: Bool)
}

