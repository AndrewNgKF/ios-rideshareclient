//
//  PassengerAnnotation.swift
//  Ride Share
//
//  Created by Drewton on 30/1/18.
//  Copyright © 2018 Andrew Ng. All rights reserved.
//

import Foundation
import MapKit

class PassengerAnnotation: NSObject, MKAnnotation {
    
    dynamic var coordinate: CLLocationCoordinate2D
    var key: String
    
    init(coordinate: CLLocationCoordinate2D, key: String) {
        self.coordinate = coordinate
        self.key = key
        super.init()
    }
    
    
    
    
    
}
