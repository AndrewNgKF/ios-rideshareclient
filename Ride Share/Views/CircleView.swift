//
//  CircleView.swift
//  Ride Share
//
//  Created by Drewton on 11/1/18.
//  Copyright © 2018 Andrew Ng. All rights reserved.
//

import UIKit

class CircleView: UIView {
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            setupView()
        }
    }
    
    
    func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderWidth = 1.5
        self.layer.borderColor = borderColor?.cgColor
    }
    
    override func awakeFromNib() {
        setupView()
    }


}
