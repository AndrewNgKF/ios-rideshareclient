//
//  RoundedCornerTextField.swift
//  Ride Share
//
//  Created by Drewton on 22/1/18.
//  Copyright © 2018 Andrew Ng. All rights reserved.
//

import UIKit

class RoundedCornerTextField: UITextField {
    
    var textRectOffset : CGFloat = 20

    override func awakeFromNib() {
        setupView()
    }
    
    func setupView() {
//        self.layer.cornerRadius = self.frame.height / 2
        self.layer.cornerRadius = 20
        self.clipsToBounds = true
    }
    

    
//    override func textRect(forBounds bounds: CGRect) -> CGRect {
//
//        return CGRect(x: 0 + textRectOffset, y: 0 + (textRectOffset / 2), width: self.frame.width - textRectOffset, height: self.frame.height + textRectOffset)
//
//    }
//
//    override func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return CGRect(x: 0 + textRectOffset, y: 0 + (textRectOffset / 2), width: self.frame.width - textRectOffset, height: self.frame.height + textRectOffset)
//    }

}
