//
//  RoundMapView.swift
//  Ride Share
//
//  Created by Drewton on 2/2/18.
//  Copyright © 2018 Andrew Ng. All rights reserved.
//

import UIKit
import MapKit

class RoundMapView: MKMapView {
    
    override func awakeFromNib() {
        setupView()
    }

    func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 10.0
        
    }
    
    

}
