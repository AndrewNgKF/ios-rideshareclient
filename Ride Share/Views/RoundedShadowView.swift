//
//  RoundedShadowView.swift
//  Ride Share
//
//  Created by Drewton on 11/1/18.
//  Copyright © 2018 Andrew Ng. All rights reserved.
//

import UIKit

class RoundedShadowView: UIView {
    
    override func awakeFromNib() {
        setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = 5.0
        
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = #colorLiteral(red: 0.3499960343, green: 0.3499960343, blue: 0.3499960343, alpha: 1)
        self.layer.shadowRadius = 5
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
    }

    

}
