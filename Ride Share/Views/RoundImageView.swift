//
//  RoundImageView.swift
//  Ride Share
//
//  Created by Drewton on 11/1/18.
//  Copyright © 2018 Andrew Ng. All rights reserved.
//

import UIKit



class RoundImageView: UIImageView {
    
    func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
    
    override func awakeFromNib() {
        setupView()
    }

   

}
